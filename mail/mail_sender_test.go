package mail

import (
	"context"
	redis2 "gitee.com/zylvcxq/gotham-utils/redis"
	"github.com/go-redis/redis/v8"
	"testing"
)

/*
   @author:zhongyang
   @date:2023/8/20
   @description:测试代码
*/

func TestSendLimit10(t *testing.T) {
	client := redis.NewClient(&redis.Options{
		Addr: "127.0.0.1:6379",
	})
	for i := 0; i < 100; i++ {
		go func() {
			redis2.LimitByLuaScript(context.Background(), client)
		}()
	}
}
