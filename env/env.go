package env

import "os"

/*
   @author:zhongyang
   @date:2023/8/8
   @description:获取环境变量
*/

const (
	EmailAuthorizeCode       = "GOTHAM_EMAIL_AUTHORIZE_CODE"
	GothamApolloEndpoint     = "GOTHAM_APOLLO_ENDPOINT"
	GothamApolloSecret       = "GOTHAM_APOLLO_SECRET"
	GothamApolloSecretCommon = "GOTHAM_APOLLO_SECRET_COMMON"
	GothamDevelopEnvironment = "GOTHAM_DEVELOP_ENVIRONMENT"
	GothamEtcdUrl            = "GOTHAM_ETCD_URL"
	GothamJaegerUrl          = "GOTHAM_JAEGER_URL"
)

// GetEmailAuthorizeCode 获取email授权码
func GetEmailAuthorizeCode() string {
	return os.Getenv(EmailAuthorizeCode)
}

// GetApolloServerURL 从环境变量中获取阿波罗url
func GetApolloServerURL() string {
	return os.Getenv(GothamApolloEndpoint)
}

// GetApolloSecret 从环境变量中获取阿波罗secret
func GetApolloSecret() string {
	return os.Getenv(GothamApolloSecret)
}

// GetApolloSecretCommon 从common环境变量中获取阿波罗secret
func GetApolloSecretCommon() string {
	return os.Getenv(GothamApolloSecretCommon)
}

// GetETCDUrl 获取ETCD地址
func GetETCDUrl() string {
	return os.Getenv(GothamEtcdUrl)
}

// GetJaegerUrl 获取ETCD地址
func GetJaegerUrl() string {
	return os.Getenv(GothamJaegerUrl)
}

func GetMode() string {
	return os.Getenv(GothamDevelopEnvironment)
}
