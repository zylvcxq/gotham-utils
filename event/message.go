package event

/*
   @author:zhongyang
   @date:2023/7/27
   @description:消息结构体定义
*/

type Message struct {
	Id         string      `json:"id"`          // 消息ID
	Topic      string      `json:"topic"`       // 主题
	RoutingKey string      `json:"routing_key"` // 对应路由
	Body       interface{} `json:"body"`        // 消息内容
}

type DelayMessage struct {
	Message
	Delay string `json:"delay"` // 延迟时间 单位ms
}
