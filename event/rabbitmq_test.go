package event

import (
	"context"
	"fmt"
	"testing"
	"time"
)

/*
   @author:zhongyang
   @date:2023/7/27
   @description:
*/

func TestRabbitMQServer_Publish(t *testing.T) {
	server, err := NewRabbitMQServer()
	if err != nil {
		fmt.Println(err)
		return
	}
	_, err = server.Publish(context.Background(), &Message{
		Id:         "1",
		Body:       "test",
		Topic:      "cron:daily-user",
		RoutingKey: "cron:daily-user",
	})
	if err != nil {
		fmt.Println(err)
	}
	//server.PublishDelay(context.Background(), &DelayMessage{
	//	Message: Message{
	//		Id:   "1",
	//		Body: "test"},
	//	Delay: 2,
	//})
}

// 延时推送
func TestDelayPublish(t *testing.T) {
	server, err := NewRabbitMQServer()
	if err != nil {
		fmt.Println(err)
		return
	}
	_, err = server.PublishDelay(context.Background(), &DelayMessage{
		Message: Message{
			Id: "1",
			Body: map[string]string{
				"user_id": "0040ecc7-7150-4bb4-9d57-7f08ff171203",
				"account": "gotham_user2GRn7wnvT",
			},
			Topic:      "gotham-common:handle-daily-user:delay",
			RoutingKey: "gotham-common:handle-daily-user:delay",
		},
		Delay: "5000",
	})
	if err != nil {
		fmt.Println(err)
	}
}

// 发送给计划管理服务测试
func TestSendToPlanManage(t *testing.T) {
	server, err := NewRabbitMQServer()
	if err != nil {
		fmt.Println(err)
		return
	}
	_, err = server.Publish(context.Background(), &Message{
		Id:         "",
		Topic:      "gotham-common:handle-daily-user",
		RoutingKey: "gotham-common:handle-daily-user",
		Body: map[string]string{
			"user_id": "0040ecc7-7150-4bb4-9d57-7f08ff171203",
			"account": "gotham_user2GRn7wnvT",
		},
	})
}

func TestName(t *testing.T) {
	if time.Now().Before(time.Now().Add(time.Minute)) {
		fmt.Println(1)
		return
	}
	fmt.Println(2)
}
