package event

/*
   @author:zhongyang
   @date:2023/7/27
   @description:消息队列抽象接口
*/

import (
	"context"
)

type MQInterface interface {
	// Publish 发布消息
	Publish(ctx context.Context, msg *Message) (string, error)
	// PublishDelay 发布延迟消息
	PublishDelay(ctx context.Context, msg *DelayMessage) (string, error)
	// Subscribe 订阅消息
	Subscribe(topic, tag, consumer string, handler interface{}) error
	// Unsubscribe 取消订阅
	Unsubscribe(topic, consumerName string) error
}

type Publisher interface {
	// Publish 发布消息
	Publish(ctx context.Context, msg *Message) (string, error)
	// PublishDelay 发布延迟消息
	PublishDelay(ctx context.Context, msg *DelayMessage) (string, error)
}
