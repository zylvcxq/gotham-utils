package log_util

import (
	"context"
	"fmt"
	"gitee.com/zylvcxq/gotham-utils/mail"
	"github.com/go-kratos/kratos/v2/log"
)

/*
   @author:zhongyang
   @date:2023/8/8
   @description:加强日志组件
*/

type GothamLogger struct {
	Helper      *log.Helper
	EMailSender mail.EMailSender
}

func NewGothamLogger(logger log.Logger, emailSender mail.EMailSender) *GothamLogger {
	return &GothamLogger{Helper: log.NewHelper(logger), EMailSender: emailSender}
}

// Errorf 打印错误的同时将错误信息通过邮箱进行通知，每小时最多通知10次
func (g *GothamLogger) Errorf(ctx context.Context, format string, a ...interface{}) {
	if g.EMailSender != nil {
		_ = g.EMailSender.SendMail(ctx, "来自Gotham的错误通知", fmt.Sprintf(format, a))
	}
	g.Helper.WithContext(ctx).Errorf(format, a...)
}

func (g *GothamLogger) Infof(ctx context.Context, format string, a ...interface{}) {
	g.Helper.WithContext(ctx).Infof(format, a)
}

func (g *GothamLogger) Debugf(ctx context.Context, format string, a ...interface{}) {
	g.Helper.WithContext(ctx).Debugf(format, a)
}
