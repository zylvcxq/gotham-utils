package log_util

import (
	"fmt"
	"testing"
)

/*
   @author:zhongyang
   @date:2023/8/11
   @description:
*/

func TestChannel(t *testing.T) {
	limit := make(chan int, 10)
	for i := 0; i < 11; i++ {
		limit <- 1
	}
	fmt.Println("我没卡")
}
