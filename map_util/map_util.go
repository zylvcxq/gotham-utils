package map_util

/*
   @author:zhongyang
   @date:2023/9/5
   @description:map相关工具方法
*/

// Flatten 将多层级的map转换成单层map
func Flatten(prefix, split string, m map[string]interface{}, flatMap map[string]interface{}) {
	for k, v := range m {
		key := k
		if prefix != "" {
			key = prefix + split + k
		}

		switch val := v.(type) {
		case map[string]interface{}:
			// 如果值是一个嵌套的 map，则递归处理
			Flatten(key, split, val, flatMap)
		default:
			// 否则，将键值对添加到扁平的 Map 中
			flatMap[key] = val
		}
	}
}
